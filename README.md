Really simple element draggability (for repositioning, not drag and drop).

Attempts to deals with iframes in the page (by overlaying them with divs during the drag).

NB: To keep things simple, the function doesn't actually do the repositioning itself, but rather provides three callbacks:

1. *start*: on mousedown on a "trigger" element (default is the element itself),
2. *drag*: on mousemove; function is called with (dx, dy) values,
3. *stop*: on mouseup

In the callbacks, *this* is bound to the draggable element.

Usage
========

```javascript
draggable(elt);

draggable(elt, {
	trigger: elt,
	start: function (e) {},
	drag: function (dx, dy, delt) {},
	end: function () {}
})
```

You can customize what parts trigger the element to start dragging with the *trigger* option. It can be an element, a NodeList (ie the result of a call to querySelectAll), a string selector (which will be passed to document.querySelectAll), or an array of any of the above. In this way, you can precisely specify which parts of an element can initiate the drag (and thus conversely exclude others).



Example 1: A simple draggable
---------
Note the use of getBoundingClientRect in case the element hasn't yet been positioned (ie position is initially set by css).


```html
<style>
#mywidget {
	position: absolute; left: 200px; top: 200px; width: 320px; height: 240px; border: 1px dotted gray;
}
</style>
<div id="mywidget"></div>
```
```javascript
var elt = document.getElementById("#mywidget");
draggable(elt, {
	drag: function (dx, dy) {
		this.style.left = (parseInt(this.style.left || this.getBoundingClientRect().left) + dx) + "px";
		this.style.top = (parseInt(this.style.top || this.getBoundingClientRect().top) + dy) + "px";			
	}
});
```


Example 2: CSS Resizing
----------------------------

When using CSS *resizable*, the bottom corner becomes itself draggable. To avoid triggering resizing and dragging at the same time, draggable border elements are added (that don't overlap the bottom right corner). A .body div is added to hold the content. 

```html
<style>
#mywidget {
	position: absolute; left: 200px; top: 200px; width: 320px; height: 240px; border: 1px dotted gray;
	resize: both; overflow: hidden;
}
#mywidget .border { cursor: move; }
.bordertop { position: absolute; left: 0; top: 0; right: 0; height: 10px; }
.borderleft { position: absolute; left: 0; top: 0; bottom: 0; width: 10px; }
.borderright { position: absolute; right: 0; top: 0; bottom: 0; width: 10px; }
.borderbottom { position: absolute; left: 0; right: 0; bottom: 0; height: 10px; }
#mywidget .body {
	position: absolute;
	top: 10px; left: 10px; right: 10px; bottom: 10px
}
</style>
<div id="mywidget">
	<div class="border bordertop"></div>
	<div class="border borderleft"></div>
	<div class="border borderright"></div>
	<div class="border borderbottom"></div>
	<div class="body"></div>
</div>
```
```javascript
var elt = document.getElementById("#mywidget");

draggable(elt, {
	trigger: "#mywidget .border",
	start: function (e) {
		this.style.border = "1px solid black";
	},
	drag: function (dx, dy) {
		this.style.left = (parseInt(this.style.left || this.getBoundingClientRect().left) + dx) + "px";
		this.style.top = (parseInt(this.style.top || this.getBoundingClientRect().top) + dy) + "px";			
	},
	end: function (e) {
		this.style.border = "";
	}
});
```

Example 3: Fancy drag + resizing with constraints from 8 control points
========================================
```html
<style>
.draggable {
	position: absolute;
	left: 200px;
	top: 200px;
	width: 320px;
	height: 240px;
	border: 1px dotted gray;
	cursor: move;
}

.draggable .body {
	position: absolute;
	top: 20px; left: 20px; right: 20px; bottom: 20px;
	background-image: url('img/Divine_in_Heaven_T-shirt.jpg');
}

.resizer {
	position: relative;
	width: 20px; height: 20px;
	background: aqua;
}

.topleft {
	position: absolute; left: 0; top: 0;
	cursor: nw-resize;
}
.topcenter {
	position: absolute; left: 50%; top: 0;
	cursor: n-resize;
}
.topright {
	position: absolute; right: 0; top: 0;
	cursor: ne-resize;
}
.topcenter .resizer {
	position: relative; left: -10px;
}

.bottomleft {
	position: absolute; left: 0; bottom: 0;
	cursor: sw-resize;
}
.bottomcenter {
	position: absolute; left: 50%; bottom: 0;
	cursor: s-resize;
}
.bottomright {
	position: absolute; right: 0; bottom: 0;
	cursor: se-resize;
}
.bottomcenter .resizer {
	position: relative;
	left: -10px;
}

.centerleft {
	position: absolute; left: 0; top: 50%;
	cursor: w-resize;
}
.centerright {
	position: absolute; right: 0; top: 50%;
	cursor: e-resize;
}
.centerleft .resizer {
	position: relative;
	top: -10px;
}
.centerright .resizer {
	position: relative;
	top: -10px;
}

</style>
<h1>draggable!</h1>
<div class="draggable">
	<!-- resize handles -->
	<div class="topleft"><div class="resizer"></div></div>
	<div class="topcenter"><div class="resizer"></div></div>
	<div class="topright"><div class="resizer"></div></div>
	<div class="centerleft"><div class="resizer"></div></div>
	<div class="centerright"><div class="resizer"></div></div>
	<div class="bottomleft"><div class="resizer"></div></div>
	<div class="bottomcenter"><div class="resizer"></div></div>
	<div class="bottomright"><div class="resizer"></div></div>
	<!-- inset body -->
	<div class="body"></div>
</div>
<script>

function sleft (elt) {
	return parseInt(elt.style.left || elt.getBoundingClientRect().left);
}

function swidth (elt) {
	var w = parseInt(elt.style.width);
	if (isNaN(w)) {
		var r = elt.getBoundingClientRect();
		return (r.right - r.left);
	} else {
		return w;
	}
}
 
function stop (elt) {
	return parseInt(elt.style.top || elt.getBoundingClientRect().top);
}

function sheight (elt) {
	var h = parseInt(elt.style.height);
	if (isNaN(h)) {
		var r = elt.getBoundingClientRect();
		return (r.bottom - r.top);
	} else {
		return h;
	}
}
function toArray (x) {
	return Array.prototype.slice.call(x);
}

toArray(document.querySelectorAll(".draggable")).forEach(function (x) {
	draggable(x, {
			trigger: [x, x.querySelectorAll(".resizer")],
			start: function (e) {
				this.style.border = "1px solid black";
			},
			drag: function (dx, dy, delt) {
				// console.log("delt", delt);
				if (delt.classList.contains("resizer")) {
					// console.log("resize", delt.parentNode.getAttribute("class"));
					if (delt.parentNode.classList.contains("topleft")) {
						this.style.left = (sleft(this)+dx) + "px";
						this.style.width = (swidth(this)-dx) + "px";
						this.style.top = (stop(this) + dy) + "px";
						this.style.height = (sheight(this) - dy) + "px";
					} else if (delt.parentNode.classList.contains("topcenter")) {
						this.style.top = (stop(this) + dy) + "px";
						this.style.height = (sheight(this) - dy) + "px";
					} else if (delt.parentNode.classList.contains("topright")) {
						this.style.width = (swidth(this)+dx) + "px";
						this.style.top = (stop(this) + dy) + "px";
						this.style.height = (sheight(this) - dy) + "px";
					} else if (delt.parentNode.classList.contains("centerleft")) {
						this.style.left = (sleft(this)+dx) + "px";
						this.style.width = (swidth(this)-dx) + "px";
					} else if (delt.parentNode.classList.contains("centerright")) {
						this.style.width = (swidth(this)+dx) + "px";
					} else if (delt.parentNode.classList.contains("bottomleft")) {
						this.style.left = (sleft(this)+dx) + "px";
						this.style.width = (swidth(this)-dx) + "px";
						this.style.height = (sheight(this)+dy) + "px";
					} else if (delt.parentNode.classList.contains("bottomcenter")) {
						this.style.height = (sheight(this)+dy) + "px";
					} else if (delt.parentNode.classList.contains("bottomright")) {
						this.style.width = (swidth(this)+dx) + "px";
						this.style.height = (sheight(this)+dy) + "px";
					}
				} else {
					this.style.left = (sleft(this) + dx) + "px";
					this.style.top = (stop(this) + dy) + "px";				
				}
			},
			end: function (e) {
				this.style.border = "";
			}
	});
});
</script>
```