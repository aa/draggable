(function () {

	function extend(){
	    for(var i=1; i<arguments.length; i++)
	        for(var key in arguments[i])
	            if(arguments[i].hasOwnProperty(key))
	                arguments[0][key] = arguments[i][key];
	    return arguments[0];
	}

	function draggable (elt, opts) {
		/*
		opts: 
			trigger: selector for trigger
			callbacks: start (event), end (), drag (dx, dy)
		*/

		opts = opts || {};
		var triggers = [],
			dragging = false,
			DEBUG = false,
			blocks = [],
			last_x = 0,
			last_y = 0,
			dragElement;
		
		function blockFrames () {
			var iframes = document.querySelectorAll("iframe");
			for (var i=0, l=iframes.length; i<l; i++) {
				var f = iframes[i],
					fr = f.getBoundingClientRect(),
					b = document.createElement("div");
				blocks.push(b);
				b.style.position = "absolute";
				f.parentNode.appendChild(b);
				b.style.width = (fr.right-fr.left)+"px";
				b.style.height = (fr.bottom-fr.top)+"px";
				b.style.left = f.offsetLeft + "px";
				b.style.top = f.offsetTop + "px";
				if (DEBUG) { b.style.background = "red"; }
			}
		}

		function unblockFrames () {
			for (var i=0, l=blocks.length; i<l; i++) {
				blocks[i].parentNode.removeChild(blocks[i])
			}
			blocks = [];
		}

		document.addEventListener("mouseup", mouseup, false);
		document.addEventListener("mousemove", mousemove, false);

		// touch
		function nullfn () {}
		document.addEventListener("touchstart", nullfn);
		document.addEventListener("touchmove", mousemove);
		document.addEventListener("touchend", function (e) {
			if (dragging) { end_drag(); }
		});
		document.addEventListener("touchcancel", nullfn);
		// pointermove: weirdly this IS IMPORTANT (at least for Chrome/Linux -- otherwise I get no touchmove events fired!)
		document.addEventListener("pointermove", nullfn);

		function start_drag (e, telt) {
			dragElement = telt;
			blockFrames();
			dragging = true;
			last_x = e.clientX;
			last_y = e.clientY;
			if (opts.start) { opts.start.call(elt, e); }
			e.preventDefault(); // MM nov 2016: seems to stop page selection
		}

		function end_drag() {
			dragElement = undefined;
			dragging = false;
			unblockFrames();
			if (opts.end) { opts.end.call(elt); }
		}

		function mousemove (e) {
			if (dragging) {
				e = e.touches ? e.touches[0] : e;
				var dx = e.clientX - last_x,
					dy = e.clientY - last_y;
				last_x = e.clientX;
				last_y = e.clientY;
				if (opts.drag) {
					opts.drag.call(elt, dx, dy, dragElement);
				}
			}
		}

		function mouseup (e) {
			if (dragging) { end_drag(); }
		}

		//////////////////////////////////////
		// Connect to trigger

		function process_trigger (x, fn) {
			// console.log("process_trigger", x);
			if (typeof(x) == "string") {
				if (x === ".") {
					// . alone means the element itself
					fn(elt);
				} else if (x.indexOf(". ") == 0) {
					// ". .foo" = process selector on elt
					process_trigger(elt.querySelectorAll(x.substr(2)), fn);
				} else {
					process_trigger(document.querySelectorAll(x), fn);
				}
			} else if (x.length) {
				for (var i=0, l=x.length; i<l; i++) {
					process_trigger(x[i], fn);
				}
			} else {
				fn(x)
			}
		}

		if (opts.trigger) {
			process_trigger(opts.trigger, function (x) { triggers.push(x) });
		} else {
			triggers.push(elt);
		}
		triggers.forEach(function (telt) {
			telt.addEventListener("mousedown", function (e) {
				if (!dragging && e.target==telt) { start_drag(e, telt); }
			}, false);
			telt.addEventListener("touchstart", function (e) {
				if (!dragging && e.target==telt) { start_drag(e, telt); }
			});
		});
	}
	window.draggable = draggable;


	/* helper fns */
	
	function sleft (elt) {
		return parseInt(elt.style.left || elt.getBoundingClientRect().left);
	}

	function swidth (elt) {
		var w = parseInt(elt.style.width);
		if (isNaN(w)) {
			var r = elt.getBoundingClientRect();
			return (r.right - r.left);
		} else {
			return w;
		}
	}
	 
	function stop (elt) {
		return parseInt(elt.style.top || elt.getBoundingClientRect().top);
	}

	function sheight (elt) {
		var h = parseInt(elt.style.height);
		if (isNaN(h)) {
			var r = elt.getBoundingClientRect();
			return (r.bottom - r.top);
		} else {
			return h;
		}
	}

	// <div class="topleft"><div class="resizer"></div></div>
	// <div class="topcenter"><div class="resizer"></div></div>
	// <div class="topright"><div class="resizer"></div></div>
	// <div class="centerleft"><div class="resizer"></div></div>
	// <div class="centerright"><div class="resizer"></div></div>
	// <div class="bottomleft"><div class="resizer"></div></div>
	// <div class="bottomcenter"><div class="resizer"></div></div>
	// <div class="bottomright"><div class="resizer"></div></div>

	function ensure_edge_handles (elt, positions) {
		// ensure HTML structure given above is IN the element		
		function _ensure_handle(elt, posclass) {
			var e = elt.querySelector("."+posclass);
			if (!e) {
				e = document.createElement("div");
				e.setAttribute("class", posclass);
				elt.appendChild(e);
			}
			var r = e.querySelector(".resizer");
			if (!r) {
				r = document.createElement("div");
				r.setAttribute("class", "resizer");
				e.appendChild(r);
			}
			return e;
		}
		var poss = positions.split(" ");
		for (var i=0, l=poss.length; i<l; i++) {
			_ensure_handle(elt, poss[i]);
		}
	}

	var DEOPTS = {
		positions: "topleft topcenter topright centerleft centerright bottomleft bottomcenter bottomright",
		trigger: [".", ". .resizer"],
		start: function (e) {
			// console.log("start", this, e);
			// this.style.border = "1px solid black";
			this.classList.add("dragging");
		},
		drag: function (dx, dy, delt) {
			// console.log("delt", delt);
			if (delt.classList.contains("resizer")) {
				// console.log("resize", this, delt, delt.parentNode.getAttribute("class"));
				if (delt.parentNode.classList.contains("topleft")) {
					this.style.left = (sleft(this)+dx) + "px";
					this.style.width = (swidth(this)-dx) + "px";
					this.style.top = (stop(this) + dy) + "px";
					this.style.height = (sheight(this) - dy) + "px";
				} else if (delt.parentNode.classList.contains("topcenter")) {
					this.style.top = (stop(this) + dy) + "px";
					this.style.height = (sheight(this) - dy) + "px";
				} else if (delt.parentNode.classList.contains("topright")) {
					this.style.width = (swidth(this)+dx) + "px";
					this.style.top = (stop(this) + dy) + "px";
					this.style.height = (sheight(this) - dy) + "px";
				} else if (delt.parentNode.classList.contains("centerleft")) {
					this.style.left = (sleft(this)+dx) + "px";
					this.style.width = (swidth(this)-dx) + "px";
				} else if (delt.parentNode.classList.contains("centerright")) {
					this.style.width = (swidth(this)+dx) + "px";
				} else if (delt.parentNode.classList.contains("bottomleft")) {
					this.style.left = (sleft(this)+dx) + "px";
					this.style.width = (swidth(this)-dx) + "px";
					this.style.height = (sheight(this)+dy) + "px";
				} else if (delt.parentNode.classList.contains("bottomcenter")) {
					this.style.height = (sheight(this)+dy) + "px";
				} else if (delt.parentNode.classList.contains("bottomright")) {
					this.style.width = (swidth(this)+dx) + "px";
					this.style.height = (sheight(this)+dy) + "px";
				}
			} else {
				// console.log("DRAG");
				this.style.left = (sleft(this) + dx) + "px";
				this.style.top = (stop(this) + dy) + "px";
			}
		},
		end: function (e) {
			// this.style.border = "";
			this.classList.remove("dragging");
		}
	};

	function convert_positioning_to_top_left (elt) {
		// resets an element that might be placed via right and bottom
		var cbr = elt.getBoundingClientRect();
		elt.style.left = cbr.left + "px";
		elt.style.width = (cbr.right - cbr.left) + "px";
		elt.style.right = "";
		elt.style.top = cbr.top + "px";
		elt.style.height = (cbr.bottom - cbr.top) + "px";
		elt.style.bottom = "";
	}

	function draggable_edges (elt, opts) {
		opts = extend({}, DEOPTS, opts);
		convert_positioning_to_top_left(elt);
		ensure_edge_handles(elt, opts.positions);
		draggable(elt, opts);
	}
	draggable.edges = draggable_edges;

})()